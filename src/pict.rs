#[derive(Clone, Copy, Debug, serde::Deserialize)]
pub(crate) enum Extension {
    #[serde(rename = "avif")]
    Avif,

    #[serde(rename = "jpg")]
    Jpg,

    #[serde(rename = "webp")]
    Webp,
}

impl std::fmt::Display for Extension {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::Avif => write!(f, "avif"),
            Self::Jpg => write!(f, "jpg"),
            Self::Webp => write!(f, "webp"),
        }
    }
}

#[derive(serde::Deserialize)]
pub(crate) struct Image {
    file: String,
    delete_token: String,
    details: Details,
}

impl Image {
    pub(crate) fn file(&self) -> &str {
        &self.file
    }

    pub(crate) fn delete_token(&self) -> &str {
        &self.delete_token
    }

    pub(crate) fn width(&self) -> u32 {
        self.details.width
    }

    pub(crate) fn height(&self) -> u32 {
        self.details.height
    }
}

#[derive(serde::Deserialize)]
pub(crate) struct Details {
    pub(super) width: u32,
    pub(super) height: u32,
}

#[derive(serde::Deserialize)]
pub(crate) struct Images {
    msg: String,
    code: Option<String>,
    files: Option<Vec<Image>>,
}

impl Images {
    pub(crate) fn msg(&self) -> &str {
        &self.msg
    }

    pub(crate) fn code(&self) -> Option<&str> {
        self.code.as_deref()
    }

    pub(crate) fn files(&self) -> impl Iterator<Item = &Image> {
        self.files.iter().flat_map(|v| v.iter())
    }
}

#[derive(Clone, serde::Deserialize)]
pub(crate) struct Upload {
    upload_id: String,
}

impl Upload {
    pub(crate) fn id(&self) -> &str {
        &self.upload_id
    }
}

#[derive(serde::Deserialize)]
pub(crate) struct Uploads {
    msg: String,
    uploads: Option<Vec<Upload>>,
}

impl Uploads {
    pub(crate) fn is_err(&self) -> bool {
        self.uploads.is_none()
    }

    pub(crate) fn message(&self) -> &str {
        &self.msg
    }

    pub(crate) fn files(&self) -> impl Iterator<Item = &Upload> {
        self.uploads.iter().flat_map(|v| v.iter())
    }
}
