pub enum ButtonKind {
    Submit,
    Outline,
    Plain,
}
