use ructe::Ructe;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    dotenv::dotenv().ok();
    let mut ructe = Ructe::from_env()?;
    let mut statics = ructe.statics()?;
    statics.add_sass_file("scss/layout.scss")?;
    statics.add_files("static")?;
    ructe.compile_templates("templates")?;

    Ok(())
}
