{ lib
, nixosTests
, rustPlatform
}:

rustPlatform.buildRustPackage {
  pname = "pict-rs-aggregator";
  version = "0.5.0";
  src = ./.;
  cargoLock.lockFile = ./Cargo.lock;

  nativeBuildInputs = [ ];

  passthru.tests = { inherit (nixosTests) pict-rs-aggregator; };

  meta = with lib; {
    description = "A simple image hosting service";
    homepage = "https://git.asonix.dog/asonix/pict-rs-aggregator";
    license = with licenses; [ agpl3Plus ];
  };
}
