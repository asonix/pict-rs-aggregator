(function(fn) {
    if (document.readyState === "complete" || document.readyState === "interactive") {
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
})(function() {
    var container = document.getElementById("file-input-container");
    var text = container.getElementsByTagName("span")[0];
    var input = container.getElementsByTagName("input")[0];
    if (!text || !input) {
        console.err("Error fetching file upload");
        return;
    }

    input.addEventListener("change", function(event) {
        if (event.target.files && event.target.files.length === 1) {
            console.log(event.target.files[0]);
            text.textContent = "Selected: " + event.target.files[0].name;
        }
    });
});
