# pict-rs-aggregator
_a simple image aggregator for pict-rs_

## Usage
### Running
```
$ pict-rs-aggregator -h
A simple image aggregation service for pict-rs

Usage: pict-rs-aggregator [OPTIONS]

Options:
  -a, --addr <ADDR>
          The address and port the server binds to [env: PICTRS_AGGREGATOR_ADDR=] [default: 0.0.0.0:8082]
  -u, --upstream <UPSTREAM>
          The url of the upstream pict-rs server [env: PICTRS_AGGREGATOR_UPSTREAM=] [default: http://localhost:8080]
  -d, --database-path <DATABASE_PATH>
          The path to the database [env: PICTRS_AGGREGATOR_DATABASE=] [default: sled/db-0-34]
  -s, --sled-cache-capacity <SLED_CACHE_CAPACITY>
          The amount of RAM, in bytes, that sled is allowed to consume. Increasing this value can improve performance [env: PICTRS_AGGREGATOR_SLED_CACHE_CAPACITY=] [default: 67108864]
  -c, --console-event-buffer-size <CONSOLE_EVENT_BUFFER_SIZE>
          The number of events to buffer in console. When unset, console is disabled [env: PICTRS_AGGREGATOR_CONSOLE_EVENT_BUFFER_SIZE=]
  -o, --opentelemetry-url <OPENTELEMETRY_URL>
          URL for the OpenTelemetry Colletor [env: PICTRS_AGGREGATOR_OPENTELEMETRY_URL=]
  -h, --help
          Print help information
  -V, --version
          Print version information
```

#### Examples
Running on all interfaces, port 8082, aggregatoring to localhost:8080
```
$ ./pict-rs-aggregator
```
Running locally, port 9000, aggregatoring to localhost:4000, with a database in /tmp
```
$ ./pict-rs-aggregator -a 127.0.0.1:9000 -u localhost:4000 -d /tmp/
```

## Contributing
Feel free to open issues for anything you find an issue with. Please note that any contributed code will be licensed under the AGPLv3.

## License

Copyright © 2021 Riley Trautman

pict-rs-aggregator is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

pict-rs-aggregator is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. This file is part of pict-rs.

You should have received a copy of the GNU General Public License along with pict-rs. If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

